// Five Button Piano
// by @arduinoenigma
// 2018
// uses DZL synth library, available at:
// https://github.com/dzlonline/the_synth

// Hardware connections for Audio Out:

//                     +10µF
// PIN 11 ---[ 1k ]--+---||--->> Audio out
//                   |
//                  === 10nF
//                   |
//                  GND

// DZL 2014
// HTTP://dzlsevilgeniuslair.blogspot.dk
// HTTP://illutron.dk

#include <synth.h>
#include <LiquidCrystal.h>

LiquidCrystal lcd(2, 3, 4, 5, 6, 7);
synth edgar;

//loud: 2200
//      4300

const byte kC = 1;
const byte kD = 2;
const byte kE = 4;
const byte kF = 8;
const byte kG = 16;

//key frequency assignments

//Sharps
//int freqs[] = {2217, 2489, 2959, 3322, 3729};

//CDEFG
int freqs[] = {2093, 2349, 2637, 2793, 3135};

//built in songs

//sweep up and down
//byte song1[] = {9, kC, kD, kE, kF, kG, kF, kE, kD, kC};

//happy birthday
byte song1[] = {24, kC, kC, kD, kC, kF, kE, kC, kC, kD, kC, kG, kF, kC, kC, kG, kF, kE, kD, kG, kG, kF, kE, kD, kC};

//unknown rythm
//byte song1[] = {11,kC, kD, kE, kF, kG, kF, kE, kD, kC, kG, kC};

//selling song
//byte song1[] = {11, kC, kD, kE, kD, kC, kC, kD, kE, kD, kC, kD};

//christmas, work in progress
//byte song1[] = {38, kE, kE, kE, kE, kE, kE, kE, kG, kC, kD, kE, kF, kF, kF, kF, kF, kE, kE, kE, kE, kD, kD, kD, kE, kD, kG, kE, kE, kE, kE, kE, kE, kE, kG, kC, kD, kE};

void sweep()
{
  float f = 1000;

  for (int i = 0; i < 1000; i++)
  {
    Serial.println(f);
    edgar.setFrequency(0, f);
    edgar.trigger(0);
    delay(200);
    f += 100;
  }
}

void lightup(byte value)
{
  //Serial.println(value);

  if (value & 1)
  {
    pinMode(A1, OUTPUT);
    digitalWrite(A1, LOW);
  }
  if (value & 2)
  {
    pinMode(A2, OUTPUT);
    digitalWrite(A2, LOW);
  }
  if (value & 4)
  {
    pinMode(A3, OUTPUT);
    digitalWrite(A3, LOW);
  }
  if (value & 8)
  {
    pinMode(A4, OUTPUT);
    digitalWrite(A4, LOW);
  }
  if (value & 16)
  {
    pinMode(A5, OUTPUT);
    digitalWrite(A5, LOW);
  }
}

void alloff()
{
  pinMode(A1, INPUT);
  digitalWrite(A1, HIGH);

  pinMode(A2, INPUT);
  digitalWrite(A2, HIGH);

  pinMode(A3, INPUT);
  digitalWrite(A3, HIGH);

  pinMode(A4, INPUT);
  digitalWrite(A4, HIGH);

  pinMode(A5, INPUT);
  digitalWrite(A5, HIGH);
}

byte voice = 0;

void play(int freq)
{
  /*
    Serial.print("v: ");
    Serial.print(voice);
    Serial.print(" f: ");
    Serial.println(freq);
  */

  edgar.setFrequency(voice, freq);
  edgar.trigger(voice);

  voice++;

  if (voice == 4)
  {
    voice = 0;
  }
}

void setup()
{
  Serial.begin(250000);

  lcd.begin(16, 2);

  lcd.print("Not A Jup-8");
  lcd.setCursor(0, 1);
  lcd.print(millis() / 1000);

  edgar.begin(CHA);

  edgar.setupVoice(0, SINE, 60, ENVELOPE0, 100, 64);
  edgar.setupVoice(1, SINE, 60, ENVELOPE0, 100, 64);
  edgar.setupVoice(2, SINE, 60, ENVELOPE0, 100, 64);
  edgar.setupVoice(3, SINE, 60, ENVELOPE0, 100, 64);

  //pinMode(A5, INPUT_PULLUP);  // set pull-up on analog pin A5

  alloff();

  pinMode(A0, INPUT);
}

byte ppA5 = 0;
byte ppA4 = 0;
byte ppA3 = 0;
byte ppA2 = 0;
byte ppA1 = 0;

byte songstep = 1;
byte nextvalue = 0;
byte highscore = 0;

void loop()
{
  byte pA5, pA4, pA3, pA2, pA1;
  int pA0;

  int pbend;

  // illuminate the keys to press next
  lightup(nextvalue);

  // reads the tuning potentiometer and determines what the tuning factor is
  pA0 = analogRead(A0);
  // the raw pA0 value is 0..1024, convert to 0..500
  pbend = (pA0 / 1024.0) * 500;
  // manual fudge factor, even when turned all the way down it read 33, so we subtract that
  pbend -= 33;
  //Serial.println(pbend);

  // turn off all the key lights
  alloff();

  // update the high score on the LCD screen
  lcd.setCursor(0, 1);
  lcd.print(highscore);
  //lcd.print(millis() / 1000);
  //lcd.scrollDisplayLeft();

  // read all the keys simultaneously
  // not pressed returns 1, pressed returns 0
  pA5 = digitalRead(A5);
  pA4 = digitalRead(A4);
  pA3 = digitalRead(A3);
  pA2 = digitalRead(A2);
  pA1 = digitalRead(A1);

  // for scoring purposes, convert keys to same value in song array
  // convert each key to a power of 2 (1,2,4,8,16) by multiplying it, then add all the keys.
  // unused keys show up as fixed values 32,64,128
  // since keys not pressed return 1, if nothing is pressed, this will return 255
  // The song array   invert the final result (~)
  byte v = ~(pA1 * 1 + pA2 * 2 + pA3 * 4 + pA4 * 8 + pA5 * 16 + 32 + 64 + 128);

  Serial.println(v);

  // wait for keys to be pressed
  if (v != 0)
  {
    // if we are waiting for the next note
    if (nextvalue != 0)
    {
      // and it matches the played note
      if (v == nextvalue)
      {
        // force the else section on the topmost if to be taken next time to load the next note
        // and increment the highscore
        nextvalue = 0;
        highscore++;
      }
      else
      {
        // played note does not match the one we are waiting for, reset high score and clear lcd
        highscore = 0;
        lcd.setCursor(0, 1);
        lcd.print("          ");
      }
    }
  }
  else
  {
    // load the next note
    if (nextvalue == 0)
    {
      nextvalue = song1[songstep];
      songstep++;

      // if song is over, re-start from begining
      if (songstep > song1[0])
      {
        songstep = 1;
      }
    }
  }

  // slightlyt repetitive section to detect if a key was just pressed.
  // (0 is pressed, 1 is not pressed)
  if (pA5 == 0)
  {
    // if this flag is 0, the key was not pressed last time we were here
    // i.e. this is the first time it is pressed
    if (ppA5 == 0)
    {
      // change the flag to 1 to ignore it until it is released
      ppA5 = 1;
      // and play the corresponding note using the Round Robin Algorithm
      play(freqs[4] + pbend);
    }
  }
  else
  {
    // once the key is released, clear the flag
    // so next time the key is pressed the note will play
    ppA5 = 0;
  }

  if (pA4 == 0)
  {
    if (ppA4 == 0)
    {
      ppA4 = 1;
      play(freqs[3] + pbend);
    }
  }
  else
  {
    ppA4 = 0;
  }

  if (pA3 == 0)
  {
    if (ppA3 == 0)
    {
      ppA3 = 1;
      play(freqs[2] + pbend);
    }
  }
  else
  {
    ppA3 = 0;
  }

  if (pA2 == 0)
  {
    if (ppA2 == 0)
    {
      ppA2 = 1;
      play(freqs[1] + pbend);
    }
  }
  else
  {
    ppA2 = 0;
  }

  if (pA1 == 0)
  {
    if (ppA1 == 0)
    {
      ppA1 = 1;
      play(freqs[0] + pbend);
    }
  }
  else
  {
    ppA1 = 0;
  }
}

